import React, { Component, PropTypes } from 'react';
import Radium from 'radium';
import { buttonStyles } from '../../../_common/styles';

const plusIcon = require('../../../../assets/images/plus-white.svg');

@Radium
export default class CustomButton extends Component {

  static propTypes = {
    onAddClick: PropTypes.func.isRequired,
    onFetchSimilarProduct: PropTypes.func.isRequired,
    onRemoveClick: PropTypes.func.isRequired
  };

  constructor (props) {
    super(props);
    this.state = {
      stateBtn: props.onFetchSimilarProduct()
    };
    const sProduct = props.onFetchSimilarProduct();
    console.log('constructor', sProduct);
  }

  render () {
    // const { onClick } = this.props;
    return (
      this.state.stateBtn === 1 ? (
      <button style={[ buttonStyles.base, buttonStyles.small, buttonStyles.blue ]} onClick={(e) => { this.setState({ stateBtn: 2 }); this.props.onAddClick(e); }}>
        <span style={{ paddingRight: '9px' }}>
        <img src={plusIcon}/></span>{'Add similar'}
      </button>
    ) : (
      <button style={[ buttonStyles.base, buttonStyles.small, buttonStyles.pink ]} onClick={() => { this.setState({ stateBtn: 1 }); this.props.onRemoveClick(); }}>
        <span style={{ paddingRight: '9px' }}/>{'Remove similar'}
      </button>
    )
    );
  }
}
